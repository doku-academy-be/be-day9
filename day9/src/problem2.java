import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class problem2 {

    static int BottomUp(int n) {
        int a = 0;
        int b = 1;
        int hasil;
        if (n == 0 || n == 1) {
            return n;
        }
        for (int i = 2; i <= n; i++) {
            hasil = a + b;
            a = b;
            b = hasil;
        }
        return b;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan nilai:");
        int nilai = input.nextInt();
        System.out.println("Fibonacci ke-" + nilai + " = " + BottomUp(nilai));
    }
}
