
import java.util.Scanner;
import java.util.TreeMap;

public class problem4 {

    static final TreeMap<Integer, String> romawi = new TreeMap<Integer, String>();
    static {
        romawi.put(1000, "M");
        romawi.put(900, "CM");
        romawi.put(500, "D");
        romawi.put(400, "CD");
        romawi.put(100, "C");
        romawi.put(90, "XC");
        romawi.put(50, "L");
        romawi.put(40, "XL");
        romawi.put(10, "X");
        romawi.put(9, "IX");
        romawi.put(5, "V");
        romawi.put(4, "IV");
        romawi.put(1, "I");

    }

    public static final String convert(int angka) {
        int i = romawi.floorKey(angka);
        if (angka == i) {
            return romawi.get(angka);
        }
        return romawi.get(i) + convert(angka - i);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Silahkan masukkan angka: ");
        int nilai = input.nextInt();
        String hasil = convert(nilai);
        System.out.println("Angka Romawinya: "+hasil);

    }
}
