import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class problem3 {
    static int jump(int[] stone) {
        int dest = stone.length;
        int[] currentJump = new  int[dest+1];
        currentJump[0] = 0;  currentJump[1] = Math.abs(stone[1] - stone[0]);

        for (int i = 2; i < dest; i++)

        {

            int a = currentJump[i - 1] + Math.abs(stone[i] - stone[i - 1]);

            int b = currentJump[i - 2] + Math.abs(stone[i] - stone[i - 2]);

            currentJump[i] = Math.min(a, b);

        }


        return currentJump[dest-1];
    }

    public static void main(String[] args) {
        int[] stone = {30,10,60,10,60,50};
        System.out.println(jump(stone));


    }
}
