
import java.util.Scanner;

public class problem1 {

    static int TopDown(int n){

       if(n==0||n==1){
           return n;
           }
       else {
           return (TopDown(n-1)+ TopDown(n-2));
       }

    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan nilai:");
        int nilai = input.nextInt();
        System.out.println("Fibonacci ke-"+nilai+" = "+ TopDown(nilai));
    }
}
